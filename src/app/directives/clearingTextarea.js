(function () {

  'use strict';

  angular.module('hist')
    .directive('clearingTextarea', function ($rootScope) {

      return {
        restrict: 'E',
        templateUrl: '/views/directives/clearingTextarea.html',
        scope: {
          commentText: '=ngModel'
        },
        link: function (scope, element, attrs) {

          $rootScope.$on('clearTextarea', function() {
            scope.commentText = '';
          });

        }
      };
    });
})();