(function () {

  'use strict';

  angular.module('hist')
    .directive('storyCard', function ($rootScope) {

      return {
        restrict: 'E',
        templateUrl: '/views/directives/storyCard.html',
        scope: {
          story: '=ngStory',
          commentsAutoload: '=commentsAutoload',
          openSelf: '=openSelf'
        },
        link: function (scope, element, attrs) {

          scope.loggedIn = $rootScope.loggedIn;

          $rootScope.$on('loginStatusChange', function (a, e) {
            scope.loggedIn = e.status;
          });
        }
      };
    });
})();