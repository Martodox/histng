(function () {

  'use strict';

  angular.module('hist')
    .service('LoadContent', function ($facebook, $q, $rootScope, Restangular, $http, $state, AuthService) {

      var exports = {};
      var $deferred;
      var gettingContent = false;

      function facebookResolved(resource) {
        gettingContent = true;
        $rootScope.$broadcast('showSmallLoader');

        resource.get().then(function (data) {
          $deferred.resolve(data);
          gettingContent = false;
          $rootScope.$broadcast('hideSmallLoader');
        });
      }

      exports.loadMore = function (resource) {

        if (!gettingContent) {
          $deferred = $q.defer();
          if ($rootScope.loggedIn && !$rootScope.apiLogin) {
            AuthService.login().then(function () {
              facebookResolved(resource);
            });
          } else {
            facebookResolved(resource);
          }
        }
        return $deferred.promise;

      };

      return exports;

    });
})();