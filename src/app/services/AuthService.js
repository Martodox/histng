(function () {

  'use strict';

  angular.module('hist')
    .service('AuthService', function ($facebook, $q, $rootScope, Restangular, $http, $state, $filter) {

      var $login = Restangular.one('login');

      var $deferred, $inProgress = false;

      function parseLoginRequest(authResponse) {
        $inProgress = true;
        if (angular.isUndefined($deferred)) {
          $deferred = $q.defer();
        }
        if (authResponse.status === 'connected') {

          $login.post('facebook',
            {
              accessToken: authResponse.authResponse.accessToken,
              facebookID: authResponse.authResponse.userID,
              platform: 'www'
            }
          ).then(function (a) {
              $rootScope.authToken = a.authToken;
              $rootScope.user = a.user;
              $http.withCredentials = true;
              $http.defaults.headers.common.Authorization = $rootScope.authToken;
              $rootScope.loggedIn = true;
              $rootScope.apiLogin = true;
              $rootScope.$broadcast('loginStatusChange', {status: true});
              $deferred.resolve();
              $inProgress = false;
              $rootScope.$broadcast('hideSmallLoader');
            });

        } else {
          $rootScope.authToken = 'empty';
          $rootScope.loggedIn = false;
          $rootScope.user = false;
          $rootScope.$broadcast('loginStatusChange', {status: false});
          $rootScope.$broadcast('hideSmallLoader');
          $deferred.resolve();
          $inProgress = false;
        }
      }

      return {
        login: function (authResponse) {

          if (!$inProgress) {
            $rootScope.$broadcast('showSmallLoader');
            $deferred = $q.defer();

            if (angular.isUndefined(authResponse)) {
              $facebook.getLoginStatus().then(function (a) {
                parseLoginRequest(a);
              });
            } else {
              parseLoginRequest(authResponse);
            }
          }

          return $deferred.promise;
        },
        facebookStatus: function () {
          $rootScope.$broadcast('showSmallLoader');
          var deferred = $q.defer();
          $facebook.getLoginStatus().then(function (authResponse) {
            if (authResponse.status === 'connected') {
              parseLoginRequest(authResponse);
              $rootScope.loggedIn = true;
            } else {
              $rootScope.loggedIn = false;
            }
            $rootScope.apiLogin = false;
            deferred.resolve();
          });
          return deferred.promise;

        },
        logout: function () {
          var deferred = $q.defer();
          $rootScope.$broadcast('showSmallLoader');
          $login.post('logout').then(function () {

            $facebook.logout().then(function () {

              delete $rootScope.authToken;
              delete $rootScope.user;
              $http.withCredentials = false;
              $http.defaults.headers.common.Authorization = null;
              $rootScope.loggedIn = false;
              deferred.resolve();
              $rootScope.$broadcast('loginStatusChange', {status: false});
              $rootScope.$broadcast('hideSmallLoader');

              if ($filter('limitTo')($state.current.name, 13) === 'main.homepage') {
                $state.transitionTo('main.homepage.latest');
              }
            });

          });
          return deferred.promise;
        }
      };

    });
})();