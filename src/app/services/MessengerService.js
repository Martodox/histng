(function () {

  'use strict';

  angular.module('hist')
    .service('messenger', function () {

      Messenger.options = {
        extraClasses: 'messenger-fixed messenger-on-bottom messenger-on-right',
        theme: 'flat'
      };

      function defaultSettings() {
        return {
          message: "",
          showCloseButton: true,
          hideAfter: 3
        }
      }

      var exports = {};

      exports.error = function (message) {

        if (angular.isArray(message)) {
          angular.forEach(message, exports.error);
        } else {
          var options = defaultSettings();
          options.message = message;
          options.type = 'error';
          Messenger().post(options);
        }
        return false;
      };

      exports.info = function (message) {

        if (angular.isArray(message)) {
          angular.forEach(message, exports.info);
        } else {
          var options = defaultSettings();
          options.message = message;
          options.type = 'info';
          Messenger().post(options);
        }
        return true;
      };

      return exports;


    });
})();