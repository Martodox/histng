(function () {

  'use strict';

  angular.module('hist')
    .controller('StoryController', soryController);


  function soryController($scope, $state, $rootScope, Restangular, AuthService) {


    var $story = Restangular.one('story', $state.params.id);

    $scope.loadStory = function () {
      $story.get().then(function (a) {
        $scope.story = a.story;


      });
    };

    if ($rootScope.loggedIn && !$rootScope.apiLogin) {
      AuthService.login().then(function () {
        $scope.loadStory();
      });
    } else {
      $scope.loadStory();
    }


  }

})();