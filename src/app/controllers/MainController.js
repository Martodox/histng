(function () {

  'use strict';

  angular.module('hist')
    .controller('MainController', function ($scope, $rootScope, $facebook, $log, $timeout, AuthService, Restangular, messenger) {

      var inTimeout;

      Restangular.addRequestInterceptor(function (data) {

        $timeout.cancel(inTimeout);

        $scope.smallLoader = true;

        $timeout(function () {
          $scope.smallLoader = false;
        }, 2000);

        return data;
      });

      Restangular.addResponseInterceptor(function (data) {


        if (angular.isArray(data.errors)) {
          messenger.error(data.errors);
        }

        if (angular.isArray(data.success)) {
          messenger.info(data.success);
        }

        inTimeout = $timeout(function () {
          $scope.smallLoader = false;
        }, 1000);

        return data;
      });

      $rootScope.loggedIn = false;

      $scope.smallLoader = false;
      $scope.bigLoader = false;

      $rootScope.$on('showSmallLoader', function () {
        $scope.smallLoader = true;

        $timeout(function () {
          $scope.smallLoader = false;
        }, 3000);

      });
      $rootScope.$on('hideSmallLoader', function () {
        $scope.smallLoader = false;
      });


      $scope.login = function () {
        $facebook.login().then(function (a) {
          AuthService.login(a);
        });
      };


      $scope.logout = function () {
        AuthService.logout();
      };


    });
})();