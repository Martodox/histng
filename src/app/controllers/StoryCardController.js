(function () {

  'use strict';

  angular.module('hist')
    .controller('StoryCardController', StoryCardController)
    .filter('newlines', newLinesFilter)
    .filter('htmlToPlaintext', htmlToPlaintext);


  function StoryCardController($scope, $rootScope, $state, $q, Restangular, $mdDialog, messenger) {

    $scope.call = false;
    $scope.showCommets = false;
    $scope.editMode = false;

    $scope.$watch('story', function (a) {
      if (!angular.isUndefined(a)) {
        if (!angular.equals({}, a)) {
          $scope.$emit('recieved', true);
        } else {
          $state.transitionTo('main.homepage.latest');
        }
      }
    });

    var $mark = Restangular.one('mark');

    if (angular.isUndefined($scope.commentsAutoload) || $scope.commentsAutoload != true) {
      $scope.commentsAutoload = false;
    }

    if (angular.isUndefined($scope.openSelf) || $scope.openSelf != false) {
      $scope.openSelf = true;
    }

    $scope.openStory = function () {
      if ($scope.openSelf) {

        $state.transitionTo('main.story', {id: $scope.story.id});
      }
    };

    $scope.abuse = function () {
      $mdDialog.show({
        controller: AbuseController,
        templateUrl: '/views/directives/abuseDialog.html'
      })
        .then(function (save) {

          $scope.storyResource.post('report', {reason: save});

        }, function () {
          messenger.error('Zgłoszenie zostało anulowane');
        });
    };

    $scope.edit = function () {

      if (!$scope.story.can_edit) {
        return;
      }

      $scope.tmpStory = angular.copy($scope.story);

      $scope.editMode = !$scope.editMode;
    };

    $scope.submitEdit = function () {
      if (angular.equals($scope.tmpStory, $scope.story)) {
        $scope.editMode = !$scope.editMode;
        return;
      }

      var payload = {
        id: $scope.tmpStory.id,
        content: $scope.tmpStory.content,
        title: $scope.tmpStory.title
      };
      $scope.editErrors = [];
      Restangular.one('content').post('edit', payload).then(function (response) {
        if (response.errors.length > 0) {
          $scope.editErrors = response.errors;
        } else {
          $scope.story.content = response.story.content;
          $scope.story.title = response.story.title;
          $scope.editMode = !$scope.editMode;
        }
      });

    };


    $scope.usersProfile = function (slug) {
      $state.transitionTo('main.userContent', {slug: slug});
    };

    $scope.goToCategory = function () {

      $state.transitionTo('main.homepage.category', {slug: $scope.story.category.slug});
    };

    $scope.markFavorite = function () {
      if (!$scope.call && $rootScope.loggedIn) {
        $scope.call = true;

        $scope.story.meta.favorite = 1 - $scope.story.meta.favorite;
        $mark.post('favorite', {id: $scope.story.id}).then(function (a) {
          $scope.isFav = a.res;
          if ($scope.isFav) {
            $scope.story.favorite_count++;
          } else {
            $scope.story.favorite_count--;
          }
          $scope.story.meta = a.status;
          $scope.call = false;

          $rootScope.$emit('metaChange', $scope.story);
        });
      }
    };


    $scope.archive = function () {
      if (!$scope.call) {

        $scope.call = true;

        $scope.story.meta.read = 1 - $scope.story.meta.read;

        $mark.post('read', {id: $scope.story.id}).then(function (a) {

          $scope.call = false;
          $scope.story.meta = a.status;

          $rootScope.$emit('metaChange', $scope.story);

        });
      }
    };

    $scope.$on('recieved', function () {
      $scope.storyResource = Restangular.one('story', $scope.story.id);
      if ($scope.commentsAutoload) {
        $scope.loadComments();
      }
    });

    $scope.loadComments = function () {
      if ($scope.showCommets) {
        $scope.showCommets = false;
      } else {

        $scope.storyResource.one('comments').get().then(function (a) {

          $scope.comments = [];
          $scope.comments = a.comments;
          $scope.showCommets = true;
        });
      }
    };

    $scope.isCommentValid = function (comment) {
      if (angular.isUndefined(comment) || comment.length < 3) {
        messenger.error('Niepoprawny komentarz');
        return false;
      }
      return true;
    };

    $scope.saveReply = function (reply, comment) {

      if (!$scope.isCommentValid(reply)) {
        return;
      }

      var payload = {
        story_id: $scope.story.id,
        content: reply,
        parent_id: comment.id
      };

      $scope.postComment(payload).then(function (response) {
        payload = $scope.addPostMeta(payload, response);
        comment.replies = payload.concat(comment.replies);
        $rootScope.$broadcast('clearTextarea');
      });

    };

    $scope.saveComment = function (reply) {

      if (!$scope.isCommentValid(reply)) {

        return;
      }

      var payload = {
        story_id: $scope.story.id,
        content: reply
      };

      $scope.postComment(payload).then(function (response) {
        payload = $scope.addPostMeta(payload, response);
        $scope.comments = payload.concat($scope.comments);
        $rootScope.$broadcast('clearTextarea');
      });

    };

    $scope.addPostMeta = function (payload, response) {
      payload.created_at = moment().format('YYYY-MM-DD HH:mm:ss');
      payload.id = response.comment_id;
      payload.replies = [];
      payload.user = {
        nickname: $rootScope.user.nickname,
        nickname_slug: $rootScope.user.nickname_slug,
        avatar: $rootScope.user.avatar_s
      };
      return [payload];
    };

    $scope.postComment = function (payload) {
      var deferred = $q.defer();

      $scope.storyResource.post('comments', payload).then(function (comment) {
        $scope.story.comment_count++;
        deferred.resolve(comment);
      });
      return deferred.promise;

    };

  }

  function AbuseController($scope, $mdDialog) {
    $scope.hide = function () {
      $mdDialog.hide();
    };
    $scope.cancel = function () {
      $mdDialog.cancel();
    };
    $scope.save = function (answer) {
      $mdDialog.hide(answer);
    };
  }

  function newLinesFilter() {
    return function (text) {
      if (!angular.isUndefined(text)) {
        return text.replace(/\n/g, '<br/>');
      }
    };
  }

  function htmlToPlaintext() {
    return function (text) {
      return String(text).replace(/<[^>]+>/gm, '');
    };
  }

})();