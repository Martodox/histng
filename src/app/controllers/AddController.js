(function () {

  'use strict';

  angular.module('hist')
    .controller('AddController', function ($scope, $rootScope, $timeout, $state, Restangular, messenger) {

      Restangular.one('categories').get().then(function (a) {
        $scope.categories = a;
      });

      $scope.added = false;

      $scope.saveStory = function (newStoryContent) {

        //TODO: validate
        var $payload = angular.copy(newStoryContent);

        Restangular.one('content').customPOST($payload).then(function (a) {

          if (a.status) {
            $scope.added = true;
          }

        });

      };

    });
})();