(function () {

  'use strict';

  angular.module('hist')
    .controller('NavController', function ($scope, $state, $rootScope) {

      $rootScope.$on('loginStatusChange', function () {
        $scope.user = $rootScope.user;
      });

      $scope.changePage = function (page) {
        $state.transitionTo(page);
      };

    });
})();