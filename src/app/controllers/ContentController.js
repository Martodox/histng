(function () {

  'use strict';

  angular.module('hist')
    .controller('ContentController', ContentController);


  function ContentController($scope, Restangular, $filter, $state, $timeout, LoadContent, $rootScope) {

    $scope.source = Restangular.one('content');

    $scope.previousState = '';

    var gettingContent = false;


    $scope.resetContent = function () {
      $scope.lastId = false;
      $scope.content = [];
      $scope.showEndMessage = false;
    };

    $scope.resetContent();

    $scope.getLastId = function () {
      if ($scope.content.length > 0) {
        return $scope.content[$scope.content.length - 1].id;
      } else {
        return -1;
      }
    };


    $scope.stickContent = function (content) {
      gettingContent = false;
      $scope.content = $scope.content.concat(content.content);

      if ($scope.lastId == $scope.getLastId()) {
        gettingContent = true;
        $scope.showEndMessage = true;
      } else {
        $scope.lastId = $scope.getLastId();
      }

    };

    $scope.$on('$stateChangeStart', function (event, toState, toParams) {
      if ($filter('limitTo')(toState.name, 13) === 'main.homepage') {
        gettingContent = false;
        $scope.lastId = -1;
        $scope.setSource(toState, toParams);
      }
    });


    $scope.handlehomePage = function (part) {

      if ($scope.lastId) {
        var res = $scope.source.one(part + '/' + $scope.lastId);
      } else {
        var res = $scope.source.one(part);
      }

      LoadContent.loadMore(res).then(function (content) {
        $scope.stickContent(content);
      });

    };

    $scope.handleCategory = function () {

      if (angular.isUndefined($scope.params)) {
        $state.transitionTo('main.homepage.latest');
        gettingContent = false;

      } else {

        if ($scope.lastId) {
          var slug = 'category/' + $scope.params.slug + '/' + $scope.lastId;
        } else {
          var slug = 'category/' + $scope.params.slug;
        }

        LoadContent.loadMore($scope.source.one(slug)).then(function (content) {
          $scope.stickContent(content);
        });
      }

    };

    $scope.handleUser = function () {

      var link = $scope.source.one('profile').one($scope.params.slug);

      if ($scope.lastId) {
        var link = link.one('posts/' + $scope.lastId);
      } else {
        var link = link.one('posts');
      }


      LoadContent.loadMore(link).then(function (content) {
        $scope.stickContent(content);
      });

    };

    $scope.setSource = function (state, params) {

      if (!gettingContent) {

        gettingContent = true;

        $scope.currentState = state;
        $scope.params = params;

        if ($scope.previousState !== $scope.currentState) {
          $scope.previousState = $scope.currentState;
          $scope.resetContent();
          //refresh
        }

        switch ($scope.currentState.name) {
          case 'main.homepage.latest':
            $scope.handlehomePage('latest');
            break;
          case 'main.homepage.read':
            $scope.handlehomePage('read');
            break;
          case 'main.homepage.favorite':
            $scope.handlehomePage('favorite');
            break;
          case 'main.homepage.category':
            $scope.handleCategory();
            break;
          case 'main.userContent':
            $scope.handleUser();
            break;
          default:
            $state.transitionTo('main.homepage.latest');
        }
      }
    };

    $scope.loadMore = function () {
      $scope.setSource($state.current, $state.params);
    };

    $scope.hasOwnValue = function (needle, haystack) {
      for (var prop in haystack) {
        if (haystack.hasOwnProperty(prop) && haystack[prop] === needle) {
          return true;
        }
      }
      return false;
    };


    $rootScope.$on('metaChange', function (a, story) {

        var state = {};
        state.favorite = 'main.homepage.favorite';
        state.read = 'main.homepage.read';
        state.latest = 'main.homepage.latest';

        if (!$scope.hasOwnValue($scope.currentState.name, state)) {
          return;
        }


        if ($scope.currentState.name == state.latest) {
          if (story.meta.favorite == 1 || story.meta.read == 1) {

            $scope.content.splice($scope.content.indexOf(story), 1);

          }
        }

        if ($scope.currentState.name == state.read) {
          if (story.meta.read == 0) {

            $scope.content.splice($scope.content.indexOf(story), 1);

          }
        }

        if ($scope.currentState.name == state.favorite) {
          if (story.meta.favorite == 0) {

            $scope.content.splice($scope.content.indexOf(story), 1);

          }
        }
      }
    );

    $rootScope.$on('loginStatusChange', function () {
      $scope.resetContent();
      $scope.setSource($state.current, $state.params);
    });

    $scope.setSource($state.current, $state.params);

  }


})
();