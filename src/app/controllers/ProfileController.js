(function () {

  'use strict';

  angular.module('hist')
    .controller('ProfileController', profileController)
    .controller('ProfileControllerContent', profileControllerContent);


  function profileController($scope, $rootScope, $state, AuthService, Restangular) {

    var $user = Restangular.one('profile');

    $scope.previewLink = false;
    $scope.errors = [];
    $scope.profileLoaded = false;

    $scope.$watch('image', function (a) {
      if (!angular.isUndefined(a)) {
        $scope.previewLink = 'data:' + a.filetype + ';base64,' + a.base64;
      } else {
        $scope.previewLink = 'http://www.keita-gaming.com/assets/profile/default-avatar-c5d8ec086224cb6fc4e395f4ba3018c2.jpg';
      }

    });

    $scope.usersProfile = function () {
      $state.transitionTo('main.userContent', {slug: $scope.user.nickname_slug});
    };

    $scope.getProfile = function () {
      $scope.user = $rootScope.user;

      $user.get().then(function (a) {
        $scope.profile = a.user;
        if ($scope.profile.load_comments == 1) {
          $scope.autoCommentLoad = true;
        } else {
          $scope.autoCommentLoad = false;
        }

        if ($scope.profile.allow_notification == 1) {
          $scope.allowNotification = true;
        } else {
          $scope.allowNotification = false;
        }
        $scope.profileLoaded = true;
      });
    };


    if ($rootScope.loggedIn && !$rootScope.apiLogin) {
      AuthService.login().then(function () {
        $scope.getProfile();
      });
    } else if ($rootScope.loggedIn && $rootScope.apiLogin) {
      $scope.getProfile();
    } else {
      $state.transitionTo('main.homepage.latest');
    }


    $scope.updateProfile = function () {
      $rootScope.$broadcast('showSmallLoader');
      $scope.errors = [];
      var $autoload;
      if ($scope.autoCommentLoad) {
        $autoload = 1;
      } else {
        $autoload = 0;
      }

      var $allowNotification;
      if ($scope.allowNotification) {
        $allowNotification = 1;
      } else {
        $allowNotification = 0;
      }

      var $payload = {
        autoload: $autoload,
        allowNotification: $allowNotification
      };

      if ($scope.profile.nickname != $rootScope.user.nickname) {
        $payload.nickname = $scope.profile.nickname;
      }

      if (!angular.isUndefined($scope.image)) {
        $payload.avatar = $scope.image.base64;
      }

      $user.post('update', $payload).then(function (a) {
        if (!angular.isUndefined(a.userUpdate.avatar)) {
          $scope.profile.avatar = a.userUpdate.avatar;
          $rootScope.user.avatar = a.userUpdate.avatar;
        }

        if (!angular.isUndefined(a.userUpdate.nickname)) {
          if (angular.isUndefined(a.userUpdate.nickname.errors)) {
            $rootScope.user.nickname = a.userUpdate.nickname;
            $scope.user.nickname = a.userUpdate.nickname;
          } else {
            $scope.errors = a.userUpdate.nickname.errors;
          }
        }


        $rootScope.$broadcast('hideSmallLoader');
      });
    };
  }

  function profileControllerContent($scope, $rootScope, $state, AuthService, Restangular) {

    var $profile = Restangular.one('content').one('profile').one($state.params.slug);


    $profile.get().then(function (user) {
      $scope.userProfile = user.user;
    });


  }

})();