(function () {

  'use strict';

  angular.module('hist', [
    'ngAnimate',
    'ngCookies',
    'ngTouch',
    'ngSanitize',
    'ui.router',
    'ngMaterial',
    'ngFacebook',
    'infinite-scroll',
    'duScroll',
    'restangular',
    'angularMoment',
    'ngTagsInput',
    'monospaced.elastic',
    'naif.base64',
    'toasty'
  ])
    .config(function ($stateProvider, $urlRouterProvider) {

      $stateProvider
        .state('main', {
          abstract: true,
          views: {
            '': {
              templateUrl: 'views/main.html'
            },
            'header@main': {
              templateUrl: 'views/header.html'
            }
          },
          resolve: {
            login: function ($rootScope, AuthService) {
              return AuthService.facebookStatus();
            }
          }

        })
        .state('main.homepage', {
          url: '/historie',
          abstract: true,
          views: {
            '': {
              templateUrl: 'views/homepage/homepage.html'
            }
          }
        })
        .state('main.homepage.latest', {
          url: '/najnowsze'
        })
        .state('main.homepage.read', {
          url: '/przeczytane'
        })
        .state('main.homepage.favorite', {
          url: '/ulubione'
        })
        .state('main.homepage.category', {
          url: '/kategoria/{slug}'
        })
        .state('main.story', {
          url: '/historia/{id}',
          controller: 'StoryController',
          templateUrl: 'views/story/story.html'
        })
        .state('main.add', {
          url: '/nowa',
          views: {
            '': {
              controller: 'AddController',
              templateUrl: 'views/add/add.html'
            }
          }
        })
        .state('main.profile', {
          url: '/profil',
          views: {
            '': {
              controller: 'ProfileController',
              templateUrl: 'views/profile/profile.html'
            }
          }
        })
        .state('main.userContent', {
          url: '/profil/{slug}',
          views: {
            '': {
              controller: 'ProfileControllerContent',
              templateUrl: 'views/profile/content.html'
            }
          }
        });
      $urlRouterProvider.otherwise('/historie/najnowsze');
    })
    .run(function () {
      (function () {
        if (document.getElementById('facebook-jssdk')) {
          return;
        }
        var firstScriptElement = document.getElementsByTagName('script')[0];
        var facebookJS = document.createElement('script');
        facebookJS.id = 'facebook-jssdk';
        facebookJS.src = '//connect.facebook.net/en_US/all.js';
        firstScriptElement.parentNode.insertBefore(facebookJS, firstScriptElement);
      }());
    });
})();