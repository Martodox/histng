angular.module('hist')
  .config(function (RestangularProvider) {
    RestangularProvider.setBaseUrl('URL');
  })
  .config(function ($facebookProvider) {
    $facebookProvider.setAppId('ID');
  });
