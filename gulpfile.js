var gulp = require('gulp'),
  plugins = require("gulp-load-plugins")({lazy: false}),
  wiredep = require('wiredep').stream,
  gulpFilter = require('gulp-filter'),
  bowerSrc = require('gulp-bower-src'),
  less = require('gulp-less'),
  symlink = require('gulp-symlink'),
  rename = require('gulp-rename'),
  clean = require('gulp-clean'),
  jshint = require('gulp-jshint'),
  fs = require('fs'),
  handlebars = require('gulp-compile-handlebars'),
  runSequence = require('run-sequence'),
  ngAnnotate = require('gulp-ng-annotate');


var filter = gulpFilter('**/*.js', '!**/*.min.js', '!**/*.css');

gulp.task('clean', function () {
  return gulp.src('assets')
    .pipe(clean());
});

gulp.task('scripts-no-concat', function () {
  return gulp.src('./src/scripts/**')
    .pipe(gulp.dest('./assets/scripts'));
});

gulp.task('config', function () {
  return gulp.src('./src/config/config.js')
    .pipe(ngAnnotate())
    .pipe(gulp.dest('./assets/app'));
});

gulp.task('scripts', ['config'], function () {
  return gulp.src('./src/app/**')
    .pipe(ngAnnotate())
    .pipe(gulp.dest('./assets/app'));
});


gulp.task('templates', function () {
  return gulp.src('./src/views/**')
    .pipe(gulp.dest('./assets/views'));
});

gulp.task('css', function () {
  return gulp.src('./src/styles/compiled/*.css')
    .pipe(plugins.concat('app.css'))
    .pipe(gulp.dest('./assets'));
});

gulp.task('lint', function () {
  return gulp.src('./src/app/**/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
});

gulp.task('less', function () {
  return gulp.src('./src/styles/main.less')
    .pipe(less({
      errLogToConsole: true
    }))
    .pipe(gulp.dest('./assets/'));
});

gulp.task('bower-components', function () {
  return bowerSrc()
    .pipe(filter)
    .pipe(gulp.dest('./assets/bower_components'));
});

gulp.task('vendorJS', function () {
  //concatenate vendor JS files
  return gulp.src(['!./bower_components/**/*.min.js',
    './bower_components/**/*.js'])
    .pipe(plugins.concat('lib.js'))
    .pipe(gulp.dest('./assets'));
});

gulp.task('vendorCSS', function () {
  //concatenate vendor CSS files
  return gulp.src(['!./bower_components/**/*.min.css',
    //do not use any bootstrap file from bower_components,
    // because there are lot of it and its components has different names
    // (bootstrap, bootstrap-css, boostrap-sass-official).
    // Bootstrap is included in controllers.scss
    './bower_components/*/*.css', './bower_components/*/dist/*.css', './bower_components/*/lib/*.css', './bower_components/*/css/*.css', '!./bower_components/**/bootstrap*'])
    .pipe(plugins.concat('lib.css'))
    //.pipe(cssfilter)
    .pipe(gulp.dest('./assets'));
});

gulp.task('wiredep', function () {

  return gulp.src('./src/index.hbs')
    .pipe(wiredep({}))
    .pipe(gulp.dest('./assets'));

});

gulp.task('link-index', ['wiredep'], function () {
  return gulp.src('./assets/index.hbs')
    .pipe(symlink(['./assets/index.hbs'], {force: true}));
});

gulp.task('copy-assets', function () {
  return gulp.src('./src/fonts/**')
    .pipe(gulp.dest('./assets/fonts'));
  return gulp.src('./src/images/**')
    .pipe(gulp.dest('./assets/images'));
});


gulp.task('eval-handlebars', ['wiredep'], function () {
  return gulp.src('./assets/index.hbs')
    .pipe(handlebars({}, {
      helpers: {
        assetPath: function (path) {
          return path;
        }
      }
    }))
    .pipe(rename('index.html'))
    .pipe(gulp.dest('./assets'));
});

gulp.task('watch', function () {
  gulp.watch([
    'assets/**/*.html',
    'assets/**/*.js',
    'assets/**/*.css'
  ]);
  gulp.watch(['./src/**/*.js', '!./src/**/*test.js'], ['scripts']);
  gulp.watch(['!./src/index.hbs', './src/**/*.html'], ['templates']);
  gulp.watch('./src/index.hbs', ['wiredep', 'eval-handlebars']);
  gulp.watch('./src/**/*.less', ['less']);

});

gulp.task('default', ['scripts', 'templates', 'bower-components', 'vendorCSS', 'copy-assets', 'less', 'wiredep', 'eval-handlebars', 'watch']);

gulp.task('check', ['lint']);


gulp.task('build', function () {
  runSequence('clean',
    ['scripts', 'templates', 'bower-components', 'vendorCSS', 'copy-assets', 'less', 'wiredep', 'eval-handlebars']);
});